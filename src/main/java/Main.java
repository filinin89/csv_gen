

public class Main {

    public static void main(String[] args) {

        int minWordLength = 4; // чтоб не было пустых полей
        int maxWordLength;
        int integerLength;
        int floatLength;
        final int dateLength = 10; // длина с учетом точек

        int numbOfColumns = Integer.valueOf(args[1]), numbOfRows = Integer.valueOf(args[3]);
        maxWordLength = Integer.valueOf(args[5]);
        integerLength = Integer.valueOf(args[7]);
        floatLength = Integer.valueOf(args[9]);
        String output = args[11];



        Generator gen = new Generator();
        FileUtils fileUtils = new FileUtils();

        // работа генератора
        String[] rColumns = gen.genColumns(numbOfColumns, maxWordLength, minWordLength);
        String[] rRows = gen.genRows(numbOfRows, numbOfColumns, maxWordLength, dateLength, integerLength, floatLength);

        fileUtils.writeCSVFile(rColumns, rRows, output);

        System.out.println("\nПрограмма успешно сгенерировала данные и записала их в файл");


    }
}
