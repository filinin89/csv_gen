
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtils {

    /**
     * Метод для записи в csv файл - позволяет записывать сгенерированные табличные данные в csv файл
     */
    public void writeCSVFile(String[] randomColumns, String[] randomRows, String outputFile){
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

            // пишем колонки
            for(int i = 0; i< randomColumns.length; i++){
                writer.write(randomColumns[i]);
            }
            writer.write("\r\n");

            // пишем ряды
            for(int i = 0; i< randomRows.length; i++){
                writer.write(randomRows[i] + "\r\n");
            }



            writer.close(); // можно закрыть поток здесь

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Couldn't write");

        }
    }
}
